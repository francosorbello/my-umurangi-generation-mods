# My Umurangi Generation mods

Small fixes and nitpicks for the game Umurangi Generation

## Instalation guide

1. Download this repository by clicking the download icon on the top right of the page.

![download-btn.png](images/download-btn.png)

2. Open the folder containing your game files. In steam, you can find it by going to your library, right clicking the game, clicking "Manage" and then selecting "Browse local files".

![browse-files.png](images/browse-files.png)

You should see 5 files, one being a folder called "Umurangi Generation_Data"

![directory.png](images/directory.png)

3. Open the folder  "Umurangi Generation_Data". Then open the folder "Managed" and copy the files previously downloaded.

Now the mods are installed! Have fun!
