# Features
- Disable the "Adjustments" UI so you can see the changes of fog / skybox / etc.

# Use

In the pause menu, press **P** on keyboard or **Y button** on gamepad to enable/disable the UI

You can watch a video of the mod here: 
https://youtu.be/B8mNTG3_EoE

# Bug fixes
- Slider selection no longer resets when you disable the photo develop ui
